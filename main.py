import ext_image
import ext_text
import os
import cv2
import matplotlib.pyplot as plt

filename = 'U05A51P3.pdf'
f_skip = 2
b_skip = 2

# Extract pages from pdf
#ext_image.ext_pages(filename)

# Extract blocks from pages    
#ext_image.ext_blocks_from_all(filename , f_skip, b_skip)

# Extract text from a block
block_name = "30__0_1.jpg"
block_path = os.path.join(ext_image.get_path(filename, "blocks"),
                          block_name)
block , text = ext_text.ext_text(block_path, True)

# Plot block and show text
plt.imshow( block)
print(text)
