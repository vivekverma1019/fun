# -*- coding: utf-8 -*-
"""
Created on Sat May  9 11:12:26 2020

@author: vivek
"""
from pdf2image import convert_from_path
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import cv2
import os
from tqdm import tqdm

def get_path(file_name, sub = None):
    return file_name.split(".")[0]+ "_ext" + "/" + sub
    
def ext_pages(file_name):
    pages_path = os.path.join(file_name.split(".")[0] + "_ext" , "pages")
    if not os.path.exists(pages_path):
        os.makedirs(pages_path)
    images = convert_from_path(file_name)
    for num, image in tqdm(enumerate(images)):
        save_path = os.path.join(pages_path,file_name.split(".")[0] + "_" + str(num) + ".jpg")
        cv2.imwrite(save_path , np.asarray(image))

def ext_blocks(page_path):
    image = cv2.imread(page_path)
    hinit = 110
    hinc = 305-110 
    winit = 70 
    winc = 570-70
    page_num = page_path.split("/")[-1].split("_")[-1].split(".")[0]
    
    file_name = page_path.split("/")[0].split("_")[0] + ".pdf"
    blocks_path = get_path(file_name , "blocks")
    
    if not os.path.exists(blocks_path):
        os.makedirs(blocks_path)
    
    hstart = hinit
    for row in range(10):
        wstart = winit
        for col in range(3):
            block = image[hstart:hstart+hinc,
                          wstart:wstart+winc,
                          :]
            cv2.imwrite(os.path.join(blocks_path,
                                     page_num + "__"
                                     +str(row) + "_"
                                     +str(col) + ".jpg"
                                     ), block)
            wstart = wstart + winc
        hstart = hstart + hinc
              
def ext_blocks_from_all(file_name , f_skip = 0,b_skip=0):
    pages_path = get_path(file_name, "pages")
    page_paths = sorted(os.listdir(pages_path))
    for i in tqdm(range(f_skip, len(page_paths)-b_skip)):
        suff = file_name.split(".")[0] + "_" + str(i) + ".jpg"
        page_path = os.path.join(pages_path, suff)
        ext_blocks(page_path)
        
