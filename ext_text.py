# -*- coding: utf-8 -*-
"""
Created on Sat May  9 12:21:08 2020

@author: vivek
"""

import pytesseract
import cv2
import os
from PIL import Image

pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'

def ext_text(image_path, thresh = False):
    if thresh:
        image = cv2.imread(image_path)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        
        #blur = cv2.GaussianBlur(gray,(5,5),0)
        _,thresh = cv2.threshold(gray,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

        filename = "{}.jpg".format("/".join(image_path.split("/")[:-1])
                                   + "/" + "tmp")
        cv2.imwrite(filename, thresh)

        text = pytesseract.image_to_string(Image.open(filename))
        os.remove(filename)
        
        return thresh ,text
    else:
        img = cv2.imread(image_path)
        text = pytesseract.image_to_string(Image.open(image_path))
        return img , text
        
